;; -*- geiser-scheme-implementation: chicken -*-
;;"CREATE TABLE IF NOT EXISTS "
#;(conc "CREATE TRIGGER IF NOT EXISTS kvt_audit_insert "
"AFTER INSERT ON kvt "
"FOR EACH ROW "
"BEGIN "
"INSERT INTO kvt_audit(ref,key,value,event) "
"VALUES(NEW.id,NEW.key,NEW.value,'INSERT');"
"END;"
"CREATE TRIGGER IF NOT EXISTS kvt_audit_update "
"AFTER UPDATE ON kvt "
"FOR EACH ROW "
"BEGIN "
"INSERT INTO kvt_audit(ref,key,value,event) "
"VALUES(NEW.id,NEW.key,NEW.value,'UPDATE');"
"END;"
"CREATE TRIGGER IF NOT EXISTS kvt_audit_delete "
"AFTER DELETE ON kvt "
"FOR EACH ROW "
"BEGIN "
"INSERT INTO kvt_audit(ref,key,value,event) "
"VALUES(OLD.ID,OLD.key,OLD.value,'DELETE');"
"END;")
;; TABLE-ALIST: Alist of table with column definitions ((TABLE . COLS) ...).
;;(define (db-hook) (db:exec (db:sql "PRAGMA foreign_keys(1)")))

(module db
    *
  (import scheme chicken)
  (use data-structures files extras srfi-1 posix)
  (use (prefix sql-de-lite db:))

  (define err-valid "Invalid")
  (define err-engine "Engine")
  (define err-exist "Non-Exist")

  (define db-name (make-parameter "database.db"))
  (define db-dir (make-parameter (get-environment-variable "DIR_TMP_DATA")))
  (define db-path (make-parameter (make-absolute-pathname (db-dir) (db-name))))
  (define db-table (make-parameter '((table ("id INTEGER PRIMARY KEY"
                                             "key TEXT NOT NULL COLLATE NOCASE"
                                             "value TEXT DEFAULT NULL"
                                             "stamp INTEGER NOT NULL")))))
  (define db-valid-long (make-parameter 'f))
  (define db-hook (make-parameter (lambda (db) 'ignore)))
  (define db-foreign (make-parameter 'f))

  (define sql-select "SELECT ")
  (define sql-create "CREATE TABLE IF NOT EXISTS ")
  (define sql-insert "INSERT INTO ")
  (define sql-delete "DELETE FROM ")
  (define sql-update "UPDATE ")
  (define sql-select "SELECT ")
  (define sql-values " VALUES ")
  (define sql-from " FROM ")
  (define sql-where " WHERE ")
  (define sql-set " SET ")
  (define sql-and " AND ")
  (define sql-or " OR ")
  (define sql-all "*")
  (define sql-foreign "PRAGMA foreign_keys(1)")
  (define sql-integrity "PRAGMA integrity_check")
  (define sql-quick "PRAGMA quick_check")
  (define sql-check "PRAGMA foreign_key_check")

  (define sqlite-string "SQLite format 3\x00") ;First 16bytes of SQLite file
  (define sqlite-size 3072) ;Empty SQLite file size
  (define sqlite-bytes 16) ;First bytes from SQLite file
  (define (sqlite-valid db) ;Perform database integrity validation on FILE;Return:cons of checks
    ;;FILE:Path to database.
    ;;Compare bytes to valid sqlite string
    ;;Exec database integrity checks
    (let ((sz (file-size (db-path)))
          (str ""))
      (or (>= sz sqlite-size) ;Initialized?
          (error 'sqlite-valid (conc err-valid "@" (db-path) ":" sz)))
      (set! str (with-input-from-file (db-path) ;;Read Format string
                  (lambda () (read-string sqlite-bytes))))
      (or (equal? str sqlite-string) ;Format string?
          (error 'sqlite-valid (conc err-valid "@" (db-path) ":" str)))

      (let ((rtn (db:exec (db:sql db (if (db-valid-long) sql-integrity sql-quick)))))
        (or (string=? "ok" (car rtn))
            (error 'sqlite-valid (conc err-valid "@" rtn))))
      (and (db-foreign)
           (let ((rtn (db:exec (db:sql db sql-check))))
             (and (not (null? rtn))
                  (error 'sqlite-valid (conc err-valid "@" rtn)))))))

  (define engines '(sqlite))

  (define (get-table table) ;TABLE:Key from db-table.Return:ALIST of KEY
    (cadr (assoc table (db-table))))
  (define (get-sql stmt) ;STMT:List of sql statements to be interspersed.Return:SQL string.
    (string-intersperse stmt ","))
  (define (get-cols table) ;Columns from TABLE of db-table
    (map (lambda (s)
           (string->symbol (car (string-split s))))
         (cdr (get-table table))))
  (define (value-gen e) ;SQL VALUE Generator; (col . "value") => "col='value'"; value => "'value'"
    (if (pair? e)
        (conc (car e) "='" (if (list? (cdr e)) (cadr e) (cdr e)) "'")
        (conc "'" e "'")))
  (define (col-val-join v c) ;Join ("values" ...) (cols ...) into cons (zip)
    (apply map cons c (list v)))

  (define (db-exec sql #!optional query)
    (handle-exceptions exn
        (begin
          (and (db:sqlite-exception? exn)
               (fprintf (current-error-port) "~A~%~A~%"
                        ((condition-property-accessor 'sqlite 'message) exn)
                        ((condition-property-accessor 'exn 'arguments) exn)))
          ;;((current-exception-handler) exn)
          ;;(abort (make-property-condition 'exn 'message #f))
          (abort exn))
      (db:call-with-database
       (db-path)
       (lambda (db)
         (db-hook db)
         (db-valid db)
         (cond
          ((procedure? sql)
           (sql db))
          (query
           (db:query query (db:sql db sql)))
          ('t
           (db:exec (db:sql db sql))))))))
  (define (db-create table) ;TABLE:Key from db-table.Generate and execute create table SQL.Return:DB exec
    (let ((stmt-create (conc sql-create table))
          (stmt-table (get-sql (get-table table))))
      (db-exec (conc stmt-create "(" stmt-table ");"))))
  (define (db-valid db) ;Validate database;Return:Boolean
    (and (file-exists? (db-path))
         (> (file-size (db-path)) 0)
         (cond
          ((member 'sqlite engines)
           (sqlite-valid db))
          ('t
           (error 'db-valid (conc err-engine ":" (db-path)))))))
  (define (db-schema) ;Return:SQL of DB schema
    (db-exec db:schema))
  (define (db-insert table kv) ;TABLE:Key from db-table.KV:AList of (column . value).Return:DB Insertion count
    (let* ((sql-insert (conc sql-insert table))
           (cols (if (atom? (car kv)) ;;((col . value)); (values ..)
                     (take (get-cols table) (length kv))
                     (map car kv)))
           (kv (map (lambda (e) ;;((col . value)); (values ...); (value values (col . value))
                      (if (atom? e) e (cdr e)))
                    kv)))
      (db-exec (string-translate* (conc sql-insert (intersperse cols ",")
                                        sql-values (intersperse (map value-gen kv) ",") ";")
                                  '(("((" . "(") ("))" . ")"))))))
  (define (db-select table #!optional kv) ;TABLE:Key from db-table
    (let* ((cols (get-cols table))
           (colk (and kv (atom? kv) (not (number? kv)) (list kv)))
           (val (and kv
                     (cond
                      ((not (atom? kv))
                       (or (and (dotted-list? kv)
                                (value-gen kv)) ;; col='value'
                           (string-intersperse (map value-gen (col-val-join kv cols)) ","))) ;; (col='value')
                      ((number? kv)
                       (conc "id='" kv "'"))
                      ('t #f)))))
      (db-exec (conc sql-select
                     (if colk
                         (intersperse colk ",")
                         sql-all)
                     sql-from table
                     (or (and val (conc sql-where (string-translate* val `(("," . ,sql-or))))) "")
                     ";")
               db:fetch-alists)))
  (define (db-update table key value)
    ;; UPDATE 'table SET key='key',value='value' WHERE id='1'
    (let ((set (string-intersperse
                (if (pair? (car value))
                    (map value-gen value)
                    (map value-gen
                         (zip (get-cols table) value)))
                ","))
          (where (if (number? key)
                     (conc "id=" (value-gen key))
                     (value-gen key))))
      (db-exec (conc sql-update table
                     sql-set set
                     sql-where where))))
  (define (db-delete table key)
    (db-exec (conc sql-delete table
                   sql-where (value-gen
                              (if (number? key)
                                  (cons 'id key)
                                  key)))))
  (define (db-count table #!optional sql) ;SQL COUNT
    (db-exec (conc "SELECT COUNT(*) FROM " table
                   (if sql sql ""))
             db:fetch))
  )
