;; Database (Sqlite)
;; SQL
(use test debug)
(include "db")
(import db)

(let ((f (create-temporary-file)))
  (db-dir (pathname-directory f))
  (db-name (conc (pathname-file f) "." (pathname-extension f)))
  (db-table '((kv ("id INTEGER PRIMARY KEY"
                   "key TEXT NOT NULL COLLATE NOCASE"
                   "value TEXT DEFAULT NULL"
                   "UNIQUE(key)"))))
  (db-path (make-absolute-pathname (db-dir) (db-name)))
  (test-begin)
  (test "Path" f (db-path))
  (test-group "Table"
              (test "Table" '("id INTEGER PRIMARY KEY"
                              "key TEXT NOT NULL COLLATE NOCASE"
                              "value TEXT DEFAULT NULL"
                              "UNIQUE(key)")
                    (get-table 'kv))
              (test "Table-SQL" "id INTEGER PRIMARY KEY,key TEXT NOT NULL COLLATE NOCASE,value TEXT DEFAULT NULL,UNIQUE(key)"
                    (get-sql (get-table 'kv)))
              (test "Create" 0 (db-create 'kv))
              (test "Schema" '("CREATE TABLE kv(id INTEGER PRIMARY KEY,key TEXT NOT NULL COLLATE NOCASE,value TEXT DEFAULT NULL,UNIQUE(key))")
                    (db-schema))
              (test "Insert" 1 (db-insert 'kv '((key . "key")
                                                (value . "value"))))
              (test "Insert (-colum)" 1 (db-insert 'kv '("key2" "value2")))
              (test-error "Insert (duplicate)" (db-insert 'kv '((key . "key")))) ;;UNIQUE
              ;; Error handle
              #;(test "Insert" #f (db-insert 'kv '((keyf . "key_element")
              (value . "value_element"))))

              (test "Select (all)" '(((id . 1) (key . "key") (value . "value"))
                                     ((id . 2) (key . "key2") (value . "value2")))
                    (db-select 'kv))
              (test "Select (key)" '(((key . "key"))
                                      ((key . "key2")))
                    (db-select 'kv 'key))
              (test "Select (id)" '(((id . 2) (key . "key2") (value . "value2")))
                    (db-select 'kv 2))
              (test "Insert (add)" 1 (db-insert 'kv '((key . "key3")
                                                      (value . "value3"))))
              ;;(test "Select (id)" '(((key . "key_element")))
              ;;(db-select 'kv 1))
              (test "Select (alist)" '(((id . 2) (key . "key2") (value . "value2")))
                    (db-select 'kv '(key . "key2")))
              (test "Select (-column)" '(((id . 2) (key . "key2") (value . "value2")))
                    (db-select 'kv '("key2")))
              (test "Select (multiple column)" '(((key . "key"))
                                                 ((key . "key2"))
                                                 ((key . "key3")))
                    (db-select 'kv 'key))
              (test "Insert (-column -value)" 1 (db-insert 'kv '("key4")))
              (test "Insert (-column +columnt)" 1 (db-insert 'kv '("key5" (value . "value5"))))
              (test "Select (all)" '(((id . 1) (key . "key") (value . "value"))
                                     ((id . 2) (key . "key2") (value . "value2"))
                                     ((id . 3) (key . "key3") (value . "value3"))
                                     ((id . 4) (key . "key4") (value))
                                     ((id . 5) (key . "key5") (value . "value5")))
                    (db-select 'kv))
              (test "Count" '(5) (db-count 'kv))
              (test "Count (last)" '(((id . 5) (key . "key5") (value . "value5")))
                    (db-select 'kv (cons 'id (car (db-count 'kv)))))
              (test "Update" 1
                    (db-update 'kv '(key . "key") '((key . "key6") (value . "value6"))))
              (test "Select (modified)" '(((id . 1) (key . "key6") (value . "value6")))
                    (db-select 'kv 1))
              (test "Update (id)" 1
                    (db-update 'kv 1 '((key . "key7"))))
              (test "Select (modified +id)" '(((id . 1) (key . "key7") (value . "value6")))
                    (db-select 'kv 1))
              (test "Update (list)" 1
                    (db-update 'kv 1 '("key" "value")))
              (test "Select (modified list)" '(((id . 1) (key . "key") (value . "value")))
                    (db-select 'kv 1))
              (test "Delete" 1 (db-delete 'kv 5))
              (test "Count (delete +rowid)" '(4) (db-count 'kv))
              (test "Delete" 1 (db-delete 'kv '(key . "key")))
              (test "Count (delete +column)" '(3) (db-count 'kv))

              ;;(test "Select (specified)" '(((id . 3) (key . "key3_element") (value . "value3_element")))
              ;;      (db-select 'kv '(key . "key3_element")))
              ;;(test "Insert (-column)" 1
              ;;      (db-insert 'kv '("key5_element" "value5_element")))
              ;;(test "Insert (-column -value)" 1 (db-insert 'kv '("key6_element")))
              ;;(test "Insert (duplicate)" 0 (entry "key3_element" "value3_element"))

              #; (conc "CREATE TRIGGER IF NOT EXISTS kvt_audit_insert "
              "AFTER INSERT ON kvt "
              "FOR EACH ROW "
              "BEGIN "
              "INSERT INTO kvt_audit(ref,key,value,event) "
              "VALUES(NEW.id,NEW.key,NEW.value,'INSERT');"
              "END;"
              "CREATE TRIGGER IF NOT EXISTS kvt_audit_update "
              "AFTER UPDATE ON kvt "
              "FOR EACH ROW "
              "BEGIN "
              "INSERT INTO kvt_audit(ref,key,value,event) "
              "VALUES(NEW.id,NEW.key,NEW.value,'UPDATE');"
              "END;"
              "CREATE TRIGGER IF NOT EXISTS kvt_audit_delete "
              "AFTER DELETE ON kvt "
              "FOR EACH ROW "
              "BEGIN "
              "INSERT INTO kvt_audit(ref,key,value,event) "
              "VALUES(OLD.ID,OLD.key,OLD.value,'DELETE');"
              "END;")
              )
  (test-end)
  (and (file-exists? f)
       (delete-file f)))
(test-exit)
